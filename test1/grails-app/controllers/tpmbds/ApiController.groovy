package tpmbds

import grails.converters.JSON
import grails.converters.XML

class ApiController {

    SaleAdService saleAdService

    def index() { }

    def saleAd(){
        switch(request.getMethod())
        {
            case "GET" :
                if (!params.id)
                    return response.status = 400
                def saleAdInstance = SaleAd.get(params.id)
                if (!saleAdInstance)
                    return response.status = 404
                response.withFormat{
                    xml { render saleAdInstance as XML }
                    json { render saleAdInstance as JSON }
                }
            break
            case "PUT":
                if (!params.id)
                    return response.status=400
                def saleAdInstance = SaleAd.get(params.id)
                if(!saleAdInstance)
                    return response.status=404

                def title=request.JSON.title
                saleAdInstance.title=title

                def description=request.JSON.description
                saleAdInstance.description=description

                def longDescription=request.JSON.longDescription
                saleAdInstance.longDescription=longDescription

                def price=request.JSON.price
                saleAdInstance.price=price

                saleAdService.save(saleAdInstance)
                break

            case "PATCH":
            break
            case "DELETE":
                if (!params.id)
                    return response.status = 400
                def saleAdInstance = SaleAd.get(params.id)
                if (!saleAdInstance)
                    return response.status = 404
                saleAdService.delete(saleAdInstance.id)
            break
            default:
                return response.status = 405
            break
        }
        return response.status = 406

    }

    def saleAds(){
        switch(request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400
                def saleAdInstance = SaleAd.get(params.id)
                if (!saleAdInstance)
                    return response.status = 404
                response.withFormat {
                    xml { render saleAdInstance as XML }
                    json { render saleAdInstance as JSON }
                }
                break
            case "POST":
                def saleAdInstance = new SaleAd(
                        title:  request.JSON.title,
                        description:  request.JSON.description,
                        longDescription: request.JSON.longDescription,
                        price: request.JSON.price)


                if (!saleAdInstance)
                    return response.status = 404


                def id = request.JSON.author
                def userInstance = User.get(id)

                //saleAdInstance.addToIllustrations(new Illustration(filename:"https://picsum.photos/200/300"))

                userInstance.addToSalesAd(saleAdInstance)

                userInstance.save(flush:true,failOnError:true)

                response.withFormat{
                    xml { render userInstance as XML }
                    json { render userInstance as JSON }
                }
                break
        }
    }

    def user(){
        switch(request.getMethod())
        {
            case "GET" :
                if (!params.id)
                    return response.status = 400
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                response.withFormat{
                    xml { render userInstance as XML }
                    json { render userInstance as JSON }
                }
                break
            case "PUT":
                if (!params.id)
                    return response.status=400
                def userInstance = User.get(params.id)
                if(!userInstance)
                    return response.status=404

                def title=request.JSON.title
                userInstance.title=title

                def description=request.JSON.description
                userInstance.description=description

                def longDescription=request.JSON.longDescription
                userInstance.longDescription=longDescription

                def price=request.JSON.price
                userInstance.price=price

                userService.save(userInstance)

                response.withFormat {
                    xml{render userInstance as XML}
                    json{render userInstance as JSON}

                }
                break

            case "PATCH":
                break
            case "DELETE":
                if (!params.id)
                    return response.status = 400
                def userInstance = User.get(params.id)
                if (!userInstance)
                    return response.status = 404
                userService.delete(userInstance.id)
                break
            default:
                return response.status = 405
                break
        }
        return response.status = 406

    }

    def users(){
        switch(request.getMethod()) {
            case "GET":
                if (!params.id)
                    return response.status = 400
                def usersInstance = Users.get(params.id)
                if (!usersInstance)
                    return response.status = 404
                response.withFormat {
                    xml { render usersInstance as XML }
                    json { render usersInstance as JSON }
                }
                break
            case "POST":
                if (!params.id)
                    return response.status = 400
                def usersInstance = Users.get(params.id)
                if (!usersInstance)
                    return response.status = 404

                def title = request.JSON.title
                usersInstance.title = title

                def description = request.JSON.description
                usersInstance.description = description

                def longDescription = request.JSON.longDescription
                usersInstance.longDescription = longDescription

                def price = request.JSON.price
                usersInstance.price = price

                usersInstance.create()
                break
        }
    }
}
